

 ansible-playbook -i hosts site.yml --user iawan --ask-pass


kubectl delete services,deployments,ingress --all     

kubectl delete daemonsets,replicasets,services,deployments,pods,rc,ingress --all        
		
		
kubectl get svc nginx -o yaml | grep nodePort -C 5

kubectl edit svc nginx  


kubectl delete rc nginx-ingress-controller

kubectl create -f https://k8s.io/docs/tasks/access-application-cluster/two-container-pod.yaml
kubectl get pod two-containers --output=yaml

kubectl delete --all pods --namespace=foo


kubeadm reset
  
  kubectl get deployments
   kubectl delete pods --all
   kubectl delete deployment frontend
   
   
    1116  kubectl apply -f "https://cloud.weave.works/k8s/net?k8s-version=$(kubectl version | base64 | tr -d '\n')"
	      
 1117  kubectl get pods -n kube-system -l name=weave-net -o wide
 1118  kubectl get pods -n kube-system -o wide | grep weave-net
 1119  kubectl logs weave-net-2bkfw -n kube-system weave-npc

 kubectl delete pods hello-695f76648c-86tlw --grace-period=0 --force
 
 kubectl apply -f --network-plugin=cni --network-plugin-dir=/etc/cni/net.d kuard-pod.yaml 
 
 /etc/cni/net.d/10-flannel.conflist
 
 
kubectl delete daemonsets,replicasets,services,deployments,pods,rc,ingress --all

ssh -i "k8.pem" admin@dns.eu-west-1.compute.amazonaws.com


ingress controller internal

1) after creating app (deployment / service)

2) default backend gateway (deployment / service)

3) Create nginx config to show VTS page

4) create RBAC rules (ServiceAccount)

5) nginx ingress controller deployment (deployment)

6) Create two ingress rules, one for nginx and app (type: ingress)

7) expose nginx (service)



kubectl create -f app-deployment.yaml -f app-service.yaml
kubectl create namespace ingress
kubectl create -f default-backend-deployment.yaml -f default-backend-service.yaml -n=ingress
kubectl create -f nginx-ingress-controller-config-map.yaml -n=ingress
kubectl create -f nginx-ingress-controller-deployment.yaml -n=ingress
kubectl create -f nginx-ingress.yaml -n=ingress
kubectl create -f app-ingress.yaml
kubectl create -f nginx-ingress-controller-service.yaml -n=ingress


***********pre-req**********************

First, create a systemd drop-in directory for the Docker service:

mkdir /etc/systemd/system/docker.service.d
Now create a file called /etc/systemd/system/docker.service.d/http-proxy.conf that adds the HTTP_PROXY environment variable:

[Service]
Environment="HTTP_PROXY=http://ip:8080"
If you have internal Docker registries that you need to contact without proxying you can specify them via the NO_PROXY environment variable:

Environment="HTTP_PROXY=http://ip:8080"
Environment="NO_PROXY=localhost,127.0.0.0/8,docker-registry.somecorporation.com"
Save changes:

$ sudo systemctl daemon-reload
Verify that the configuration has been loaded:

$ sudo systemctl show --property Environment docker
Environment=HTTP_PROXY=http://ip:8080
Restart Docker:

$ sudo systemctl restart docker (edited)


*************Deploying Kubernetes in AWS using KOPS ************


pre-req

* aws account
* aws cli - docs.aws.amazon.com/cli
* jq (filter json outputs) - stedolan.github.io/jq
* kops - github.com/kubernetes/kops#installing

git clone https://github.com/vfarcic/k8s-specs.git
open "https://console.aws.amazon.com/iam/home#/security_credential"

install aws cli

 1589  curl -O https://bootstrap.pypa.io/get-pip.py
 1590  python get-pip.py --userpython get-pip.py --user
 1595  export PATH=~/.local/bin:$PATH
 1596  source ~/.bash_profile
 1597  source ~/.bashrc
 1598  clear
 1599  pip install awscli
 1600  aws --version
 

export AWS_DEFAULT_REGION=eu-west-1

aws iam create-group --group-name kops
aws iam attach-group-policy --group-name kops --policy-arn arn:aws:iam::aws:policy/AmazonEC2FullAccess
aws iam attach-group-policy --group-name kops --policy-arn arn:aws:iam::aws:policy/AmazonS3FullAccess
aws iam attach-group-policy --group-name kops --policy-arn arn:aws:iam::aws:policy/AmazonVPCFullAccess
aws iam attach-group-policy --group-name kops --policy-arn arn:aws:iam::aws:policy/IAMFullAccess
aws iam create-user --user-name kops

aws iam add-user-to-group --user-name kops --group-name kops

aws iam create-access-key --user-name kops >kops-creds

kops access keys
        "SecretAccessKey": "",
        "AccessKeyId": ""

		
export AWS_ACCESS_KEY_ID=$(cat kops-creds | jq -r '.AccessKey.AccessKeyId')
export AWS_SECRET_ACCESS_KEY=$(cat kops-creds | jq -r '.AccessKey.SecretAccessKey')

aws ec2 describe-availability-zones --region $AWS_DEFAULT_REGION

export ZONES=$(aws ec2 describe-availability-zones --region $AWS_DEFAULT_REGION | jq -r '.AvailabilityZones[].ZoneName' | tr '\n' ',' | tr -d ' ')


ZONES=${ZONES%?}

echo $ZONES
eu-west-1a,eu-west-1b,eu-west-1c

mkdir -p cluster
aws ec2 create-key-pair --key-name k8 | jq -r '.KeyMaterial' > k8.pem

chmod 400 k8.pem

ssh-keygen -y -f k8.pem > k8.pub

export NAME=cluster.k8s.local
export BUCKET_NAME=k8-dev
export BUCKET_NAME=kops-state-store
 
aws s3api create-bucket --bucket $BUCKET_NAME --create-bucket-configuration LocationConstraint=$AWS_DEFAULT_REGION
aws s3api create-bucket --bucket kayan-kops-state --region eu-west-2 --create-bucket-configuration
 
aws s3api delete-bucket --bucket k8-dev --region $AWS_DEFAULT_REGION

  kops create cluster --name $NAME --vpc=vpc-id --subnets=subnet-id,subnet-id,subnet-id \
       --master-count 3 --node-count 3 --node-size t2.small --master-size t2.small --zones us-east-1a,us-east-1b,us-east-1c --master-zones \
	   us-east-1a,us-east-1b,us-east-1c --ssh-public-key k8.pub --networking kubenet --kubernetes-version v1.11.4 --yes
	   
	   kops export kubecfg --name dev.cluster.k8s.local --state s3://k8-dev
	   
kops create cluster --name $NAME --master-count 1 --node-count 2 --node-size t2.small --master-size t2.small --zones us-east-1a,us-east-1b --master-zones \
us-east-1a --ssh-public-key k8.pub --networking calico --topology private --kubernetes-version v1.11.4 --yes
	   


  kops create cluster --name $NAME --master-count 1 --node-count 1 --node-size t2.small --master-size t2.small --zones eu-west-1a --master-zones eu-west-1a --ssh-public-key k8.pub --networking kubenet --kubernetes-version v1.11.4 --yes

  
  kops create cluster --name=${CLUSTER_NAME} --vpc=vpc-1010af11 --subnets=subnet-id,subnet-id,subnet-id --master-zones=${ZONES} --zones=${ZONES}  --networking=weave
  
  
  kops get cluster
  kops validate cluster
  
				 				 
				  
kops edit cluster ${NAME} --state=s3://${BUCKET_NAME}
				  
kops delete cluster --name=${NAME} --state=s3://${BUCKET_NAME}		  
				  
kops get cluster kube.k8s.local
				  
				  
		
kops create secret --name dev.cluster.k8s.local sshpublickey admin -i ~/.ssh/id_rsa.pub
  


export BUCKET_NAME=kops-state-store 
export BUCKET_NAME=k8-dev


export NAME=dev.k8s.local
export AWS_DEFAULT_REGION=us-east-1
export BUCKET_NAME=k8-dev
export KOPS_STATE_STORE=s3://${BUCKET_NAME}
export KOPS_CLUSTER_NAME=dev.k8s.local
export AWS_ACCESS_KEY_ID=      
export AWS_SECRET_ACCESS_KEY=
export PATH=~/.local/bin:$PATH
kops export kubecfg --name dev.k8s.local --state s3://k8-dev


export NAME=dev-stack.k8s.local
export AWS_DEFAULT_REGION=us-east-1
export BUCKET_NAME=k8-dev
export KOPS_STATE_STORE=s3://${BUCKET_NAME}
export KOPS_CLUSTER_NAME=dev-stack.k8s.local
export AWS_ACCESS_KEY_ID=      
export AWS_SECRET_ACCESS_KEY=
kops export kubecfg --name dev-stack.k8s.local --state s3://k8-dev
kubectl config use-context dev-stack.k8s.local
kops validate cluster

now test your kubectl commands


vi ~/.kube/config

update number of nodes

kops edit instancegroup nodes
kops update cluster --yes

kops edit cluster dev.cluster.k8s.local

systemctl restart systemd-networkd
systemctl status systemd-networkd

setenv NAME dev.cluster.k8s.local
setenv BUCKET_NAME k8-dev


validate cluster: kops validate cluster
 * list nodes: kubectl get nodes --show-labels
 * ssh to the master: ssh -i ~/.ssh/id_rsa admin@api.dev.cluster.k8s.local
 * the admin user is specific to Debian. If not using Debian please use the appropriate user based on your OS.
 * read about installing addons at: https://github.com/kubernetes/kops/blob/master/docs/addons.md.


helm install --name dev-release --set \ 
wordpressUsername=dev_admin, \
wordpressPassword=dev_password, \
mariadb.mariadbRootPassword=dev_secretpassword \
    stable/wordpress



switch cluster
dev.cluster.k8s.local

kops export kubecfg --name dev.cluster.k8s.local --state s3://k8-dev
kops validate cluster - This updates ~/.kube/config with new cluster details
kubectl config get-contexts - should list the new cluster
kubectl config use-context CONTEXT_NAME - configure kubectl for particular cluster
kubectl config use-context -dev.cluster.k8s.local

helm --init - re-initialise helm to reference the new cluster


kubetcl config use-context <context name>

helm del --purge ingress-example


kubectl create secret docker-registry regcred --docker-server=https://registry.dns.com --docker-username=username --docker-password=pwd



helm install stable/jenkins -f custom-values.yaml --namespace jenkins


kubectl run yld-ui-deployment --replicas=2 --labels="yld-ui" --image=https://yld-registry.com/yld:latest  --port=3000 --overrides="$(kubectl get secret regcred -o json)


kubectl create secret tls tls-cert --key tls.key --cert tls.crt

kubectl exec -it jenkins-deployment-5 -- /bin/bash


kubectl apply -f jenkins-deploymen.yaml
kubectl expose deployment jenkins-deployment --port=8080 --type=ClusterIP --name=jenkins-service
kubectl apply -f -dev-ingress.yml



FROM alpine:latest
COPY "executable_file" /
ENTRYPOINT [ "./executable_file" ]
Kubernetes yaml file:

 spec:
    containers:
      - name: container_name
        image: image_name
        args: ["arg1", "arg2", "arg3"]

apiVersion: v1
kind: Pod
metadata:
  name: command-demo
  labels:
    purpose: demonstrate-command
spec:
  containers:
  - name: command-demo-container
    image: debian
    command: ["printenv"]
    args: ["HOSTNAME", "KUBERNETES_PORT"]
  restartPolicy: OnFailure		
		
		
	 helm install stable/nginx-ingress --namespace kube-system
		 
	

vi ~./kube/config

switch cluster

kops validate cluster
kubectl config get-contexts
kubectl config use-context dev.cluster.k8s.local

#######Install terraform ####################



    Install unzip

    sudo apt-get install unzip

    Download latest version of the terraform

    wget https://releases.hashicorp.com/terraform/0.11.10/terraform_0.11.10_linux_amd64.zip

    Extract the downloaded file archive

    unzip terraform_0.11.10_linux_amd64.zip

    Move the executable into a directory searched for executables

    sudo mv terraform /usr/local/bin/

    Run it

    terraform --version 




#####Install aws cli########

curl -O https://bootstrap.pypa.io/get-pip.py
python get-pip.py --user
export PATH=~/.local/bin:$PATH
source ~/.bashrc
pip install awscli
aws --version


#####Install kops########

wget https://github.com/kubernetes/kops/releases/download/1.9.0/kops-linux-amd64
chmod +x kops-linux-amd64
mv kops-linux-amd64 /usr/local/bin/kops


#######Install helm#########

apt-get install -y snap
apt-get install -y snapd
snap install helm --classic
helm init

alternate

curl https://raw.githubusercontent.com/helm/helm/master/scripts/get | bash
helm init


##########Create cluster#########

aws ec2 create-key-pair --key-name k8-key | jq -r '.KeyMaterial' > k8-key.pem
ssh-keygen -y -f k8-key.pem > k8-key.pub



kops create cluster --name -dev.k8s.local --vpc=vpc-id --subnets=subnet-id,subnet-id --master-count 1 --node-count 2 --node-size=t2.small \
--master-size t2.small --zones us-east-1a,us-east-1b --master-zones us-east-1a --ssh-public-key k8-key.pub \
--networking kubenet --kubernetes-version v1.11.4 --yes


kops create cluster --name -dev.cluster.k8s.local --vpc=vpc-id --subnets=subnet-id,subnet-id --master-count 1 --node-count 2 --node-size=t2.small \
--master-size t2.small --zones us-east-1a,us-east-1b --ssh-public-key k8-key.pub \
--networking kubenet --kubernetes-version v1.11.4 --yes


kops create cluster --name dev.cluster.k8s.local --vpc=vpc-id --subnets=subnet-id,subnet-id --master-count 1 --node-count 2 --node-size=t2.small \
--master-size t2.small --zones us-east-1a,us-east-1b --ssh-public-key k8-key.pub \
--networking kubenet --kubernetes-version v1.11.4 --yes

 * list nodes: kubectl get nodes --show-labels
 * ssh to the master: ssh -i ~/.ssh/id_rsa admin@api.dev.cluster.k8s.local



kops create cluster \
    --master-zones $ZONES \
    --zones $ZONES \
    --node-count 2 \
    --node-size=t2.small \
    --master-size=t2.small \
    --topology private \
    --networking calico \
    --vpc=$(terraform output vpc_id) \
    --target=terraform \
    --ssh-public-key k8-key.pub \
    --out=. \
    --name k8-dev.k8s.local

    ${NAME}


kops export kubecfg --name -k8-dev.k8s.local --state s3://k8-dev.dns.com-state



kubectl expose replicaset redis-deployment --port=6379 --type=ClusterIP --name=redis-service
kubectl expose replicaset postgresql-deployment --port=5432 --type=ClusterIP --name=postgresql-service
kubectl expose replicaset gitlab-deployment --type=ClusterIP --name=gitlab-service



kubectl expose replicaset postgresql-deployment --port=5432 --type=ClusterIP --name=postgresql-service
kubectl expose deployment echoserver --type=NodePort



kubectl create secret docker-registry regcred --docker-server=https://registry.dns.com --docker-username=username --docker-password=pwd



kubectl create secret tls custom-tls-cert --key /path/to/tls.key --cert /path/to/tls.crt



apiVersion: v1
kind: Pod
metadata:
  name: mc1
spec:
  volumes:
  - name: html
    emptyDir: {}
  containers:
  - name: 1st
    image: nginx
    volumeMounts:
    - name: html
      mountPath: /usr/share/nginx/html
  - name: 2nd
    image: debian
    volumeMounts:
    - name: html
      mountPath: /html
    command: ["/bin/sh", "-c"]
    args:
      - while true; do
          date >> /html/index.html;
          sleep 1;
        done


        kubectl exec -it postgresql-deployment-6 -- /bin/bash


ingress

helm install stable/nginx-ingress --namespace kube-system

for kubeadm
helm install stable/nginx-ingress --namespace kube-system --set controller.hostNetwork=true,controller.kind=DaemonSet



#################TILLER############

helm init
kubectl create serviceaccount --namespace kube-system tiller
kubectl create clusterrolebinding tiller-cluster-rule --clusterrole=cluster-admin --serviceaccount=kube-system:tiller
kubectl patch deploy --namespace kube-system tiller-deploy -p '{"spec":{"template":{"spec":{"serviceAccount":"tiller"}}}}'



kubectl apply -f config-maps/efs.yaml
kubectl apply -f deployments/efs-provisioner.yaml 
kubectl apply -f volumes/efs-vol.yaml 
kubectl apply -f deployments/jenkins-full.yaml 


kubectl get secret --namespace default grafana -o jsonpath="{.data.grafana-admin-password}" | base64 --decode ; ech

kubectl exec -it grafana-5 -- /bin/bash

       
kubectl exec -it postgresql-deployment-6 -- /bin/bash

grafana-cli admin reset-admin-password --homepath "/usr/share/grafana" pwd
		 

grafana dashboards

8588



kubectl -n kube-system delete $(kubectl -n kube-system get pod -o name | grep dashboard)


on prem (with ansible)
cp /etc/kubernetes/admin.conf ~/.kube/config


rm /etc/kubernetes/pki/apiserver.*
kubeadm alpha phase certs all --apiserver-advertise-address=0.0.0.0 --apiserver-cert-extra-sans=51.15.
docker rm -f `docker ps -q -f 'name=k8s_kube-apiserver*'`
systemctl restart kubelet

sudo kubeadm reset
sudo kubeadm init --apiserver-cert-extra-sans=51.1


https://ip/k8s/clusters/c-84l8t/api/v1/namespaces/kube-system/services/https:kubernetes-dashboard:/proxy/
